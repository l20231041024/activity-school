/**
 * 冒泡排序函数
 * 对输入的整数数组a进行升序排序
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    if (a == null || n <= 0) {
        return;
    }

    // 外层循环控制遍历次数，最多需要遍历n-1轮
    for (int i = 0; i < n - 1; i++) {
        // 内层循环用来比较并交换相邻元素，每轮结束后最大的元素会被排到末尾
        for (int j = 0; j < n - 1 - i; j++) {
            // 如果当前元素大于下一个元素，则交换它们的位置
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} // end
