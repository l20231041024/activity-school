/**
 * 冒泡排序函数
 * 使用冒泡法对数组进行升序排序
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    for (int i = 0; i < n - 1; i++) { // 外层循环控制遍历次数
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环用来比较相邻元素并可能交换它们
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
} // end
