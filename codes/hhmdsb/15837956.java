/**  
 * 冒泡排序函数  
 * 功能：对数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 遍历数组，从第一个元素到倒数第二个元素  
        for (int j = 0; j < n - 1 - i; j++) { // 每次比较相邻的元素，最后一个元素已经是当前轮的最大值，无需再比较  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
