/**  
 * 冒泡排序函数  
 * 通过相邻元素之间的比较和交换，将较大的元素逐渐“浮”到数组的末尾，从而完成排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 外层循环控制所有需要遍历的次数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环负责相邻元素间的比较和交换  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
    // 排序结束，数组a现在是有序的  
} //end

