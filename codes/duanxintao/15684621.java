/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较，将较大的元素交换到数组的末尾，  
 * 重复这个过程直到整个数组变得有序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 外层循环控制排序趟数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环控制每一趟排序多少次  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果前一个元素比后一个元素大，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
