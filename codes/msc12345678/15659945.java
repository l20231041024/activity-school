public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制所有元素遍历的次数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制相邻元素两两比较  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们的位置  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
