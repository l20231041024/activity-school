/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    // 遍历数组
    for (int i = 0; i < n; i++) {
        // 设置一个标志，用于优化（如果在一次遍历中没有发生交换，说明数组已经有序，可以提前结束排序）
        boolean flag = false;
        // 内层循环，从第一个元素到第 n-i-1 个元素
        for (int j = 0; j < n - i - 1; j++) {
            // 如果前一个元素比后一个元素大，交换它们的位置
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                // 发生了交换，标志设为 true
                flag = true;
            }
        }
        // 如果在一次遍历中没有发生交换，说明数组已经有序，可以提前结束排序
        if (!flag) {
            break;
        }
    }
} //end
