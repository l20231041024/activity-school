/**  
 * 冒泡排序函数  
 * 功能：对输入的整数数组进行冒泡排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，表示这一趟是否有交换  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，将标志位设为true  
                swapped = true;  
            }  
        }  
        // 如果在内层循环中没有发生交换，说明数组已经有序，可以退出排序  
        if (!swapped) {  
            break;  
        }  
    }  
} // end
